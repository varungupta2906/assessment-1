let hamburger=document.querySelector('.hamburger');
let mobileNav=document.querySelector('.menu2');


let bars=document.querySelectorAll('.hamburger span')

let isActive= false
hamburger.addEventListener('click',function(){
    mobileNav.classList.toggle('open')

    if(!isActive){
        bars[0].style.transform = 'rotate(45deg)'
        bars[1].style.opacity='0'
        bars[2].style.transform = 'rotate(-45deg)'
        isActive=true
    }
    else{
        bars[0].style.transform = 'rotate(0deg)'
        bars[1].style.opacity='1'
        bars[2].style.transform = 'rotate(0deg)'
        isActive= false
    }
})


// ************************************************************for LSP**********************************************************************************
let buttn=document.querySelector('.menu1-lsp-style')
let navnav= document.querySelector('.menu1-drop')
let hr=document.querySelector('.hrdrop')
let icon1=document.querySelector('.lspdrop-1')
let icon=document.querySelector('.lspdrop')
let active=false
buttn.addEventListener('click',function(){
navnav.classList.toggle('open')
if(!active){
    hr.style.opacity='0'
    navnav.style.display="block"
    icon1.style.opacity='1'
    icon1.style.display='block'
    icon.style.display='none'
    active=true
}
else{
    hr.style.opacity='1'
    navnav.style.display="none"
    icon.style.display='block'
    icon1.style.display='none'
    active= false
}

})


// ************************************************************for Company**********************************************************************************
let buttn1=document.querySelector('.menu1-lsp-style-1')
let navnav1= document.querySelector('.menu1-drop-1')
let hr1=document.querySelector('.hrdrop-1')
let iconc1=document.querySelector('.lspdropc-1')
let iconc=document.querySelector('.lspdropc')

let active1=false
buttn1.addEventListener('click',function(){
navnav1.classList.toggle('open')
if(!active1){
    hr1.style.opacity='0'
    navnav1.style.display="block"
    iconc1.style.opacity='1'
    iconc1.style.display='block'
    iconc.style.display='none'
    active1=true
}
else{
    hr1.style.opacity='1'
    navnav1.style.display="none"
    iconc.style.display='block'
    iconc1.style.display='none'
    active1= false
}

})


// ************************************************************for Resources**********************************************************************************
let buttn2=document.querySelector('.menu1-lsp-style-2')
let navnav2= document.querySelector('.menu1-drop-2')
let hr2=document.querySelector('.hrdrop-2')
let iconr1=document.querySelector('.lspdropr-1')
let iconr=document.querySelector('.lspdropr')

let active2=false
buttn2.addEventListener('click',function(){
navnav2.classList.toggle('open')
if(!active2){
    hr2.style.opacity='0'
    navnav2.style.display="block"
    iconr1.style.opacity='1'
    iconr1.style.display='block'
    iconr.style.display='none'
    active2=true
}
else{
    hr2.style.opacity='1'
    navnav2.style.display="block"
    iconr.style.display='block'
    iconr1.style.display='none'
    active2= false
}

})
